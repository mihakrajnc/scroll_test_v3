using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.UIElements;

public class UIListManager : MonoBehaviour
{
    [SerializeField] private int columnsCount;

    [SerializeField] private SpriteDatabase database;
    [SerializeField] private VisualTreeAsset listItem;

    private VisualElement root;
    private ListView listView;

    private readonly List<List<AssetReferenceAtlasedSprite>> rows = new List<List<AssetReferenceAtlasedSprite>>();

    // This  is silly but there's no nicer way to remove callbacks from a VisualElement
    private Dictionary<int, EventCallback<ClickEvent>> clickEventCache =
        new Dictionary<int, EventCallback<ClickEvent>>();

    void Start()
    {
        root = GetComponent<UIDocument>().rootVisualElement;

        // Create  columns
        var row = new List<AssetReferenceAtlasedSprite>(columnsCount);
        foreach (var sprite in database.Data)
        {
            row.Add(sprite);
            if (row.Count == columnsCount)
            {
                rows.Add(row);
                row = new List<AssetReferenceAtlasedSprite>(columnsCount);
            }
        }

        if (row.Count != 0) rows.Add(row);

        // Setup item list
        listView = root.Q<ListView>("SpriteList");
        listView.bindItem = BindItem;
        listView.makeItem = MakeItem;
        listView.unbindItem = UnbindItem;
        listView.itemsSource = rows;
    }

    private VisualElement MakeItem()
    {
        // Create a new VisualElement that holds X items horizontally
        var ve = new VisualElement();
        ve.style.flexDirection = FlexDirection.Row;
        ve.style.justifyContent = Justify.SpaceAround;

        for (int i = 0; i < columnsCount; i++)
        {
            var template = listItem.CloneTree();
            template.name = $"Label{i}";
            ve.Add(template);
        }

        return ve;
    }

    private void BindItem(VisualElement element, int index)
    {
        var row = rows[index];
        for (int i = 0; i < row.Count; i++)
        {
            var root = element.Q<VisualElement>($"Label{i}");

            // Click event
            int num = index * columnsCount + i;
            var clickEvent = clickEventCache[num] = e => { Debug.Log($"Item: {num}"); };
            root.RegisterCallback(clickEvent);

            // Set Sprite / data
            var opHandle = row[i].LoadAssetAsync();
            opHandle.Completed += handle =>
            {
                root.Q<VisualElement>("Background").style.backgroundImage = new StyleBackground(handle.Result);
            };
            opHandle.WaitForCompletion(); //  To eliminate possible race conditions - could be reworked to allow actual async loading

            // Set text
            root.Q<TextElement>("Label").text = num.ToString();
        }
    }

    private void UnbindItem(VisualElement element, int index)
    {
        var row = rows[index];
        for (int i = 0; i < row.Count; i++)
        {
            var root = element.Q<VisualElement>($"Label{i}");

            // Clear bg image
            root.Q<VisualElement>("Background").style.backgroundImage = null;

            // Clear click events
            int num = index * columnsCount + i;
            if (clickEventCache.TryGetValue(num, out var ev))
            {
                root.UnregisterCallback(ev);
                clickEventCache.Remove(num);
            }

            // Release addressable
            row[i].ReleaseAsset();
        }
    }
}