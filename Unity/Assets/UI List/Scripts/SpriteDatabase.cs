using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.AddressableAssets;

/// <summary>
/// Holds references to all the sprites.
///
/// Note: This loads all the sprites when loading the object,
/// can be improved by using Addressables / Asset Bundles.
/// </summary>
[CreateAssetMenu]
public class SpriteDatabase : ScriptableObject
{
    [SerializeField] private List<AssetReferenceAtlasedSprite> spritesAtlased;

    public List<AssetReferenceAtlasedSprite> Data => spritesAtlased;


#if UNITY_EDITOR
    [ContextMenu("Refresh Sprites")]
    private void RefreshSprites()
    {
        string[] files = Directory.GetFiles(Application.dataPath + "/Sprites/", "*.png");

        var sprites = new List<Sprite>(files.Length);
        foreach (string file in files)
        {
            sprites.Add(
                AssetDatabase.LoadAssetAtPath<Sprite>(
                    "Assets" + file.Replace(Application.dataPath, "").Replace('\\', '/'))
            );
        }

        sprites.Sort((x, y) => int.Parse(x.name).CompareTo(int.Parse(y.name)));

        spritesAtlased = new List<AssetReferenceAtlasedSprite>();
        foreach (var sprite in sprites)
        {
            var spriteAssetRef =
                new AssetReferenceAtlasedSprite(
                    AssetDatabase.AssetPathToGUID("Assets/UI List/NumbersAtlas.spriteatlas"));
            spriteAssetRef.SetEditorSubObject(sprite);

            spritesAtlased.Add(spriteAssetRef);
        }
    }
#endif
}